#!/usr/bin/env python

from setuptools import setup

setup(
    install_requires=[
        'jsonschema~=4.0',
        'aiohttp~=3.0',
        'backoff~=2.0'
    ]
)
