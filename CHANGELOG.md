# Change Log

## Unreleased
---

### New

### Changes

### Fixes

### Breaks

## [0.1.0](https://gitlab.com/singer-core/target-core/tags/0.1.0) (2022-11-09)
---

### New
* #16 Asynchronous config option by @omegax in !23

### Changes

### Fixes

### Breaks

**Full Changelog**: [`0.0.7...0.1.0`](https://gitlab.com/singer-core/target-core/compare/0.0.7...0.1.0)

## [0.0.7](https://gitlab.com/singer-core/target-core/tags/0.0.7) (2022-09-14)
---

### New

### Changes
* #15 Save Json Refactor by @omegax in !22

### Fixes

### Breaks

**Full Changelog**: [`0.0.6...0.0.7`](https://gitlab.com/singer-core/target-core/compare/0.0.6...0.0.7)

## [0.0.6](https://gitlab.com/singer-core/target-core/tags/0.0.6) (2022-09-01)
---

### New
* #14 decimal.DivisionImpossible raised when handling schema items with high levels of precision by @omegax in !21

### Changes

### Fixes

### Breaks

**Full Changelog**: [`0.0.5...0.0.6`](https://gitlab.com/singer-core/target-core/compare/0.0.5...0.0.6)

## [0.0.5](https://gitlab.com/singer-core/target-core/tags/0.0.5) (2022-08-28)
---

### New
* #13 REST API Support, S3 removal & Improvements by @omegax in !20

### Changes

### Fixes

### Breaks

**Full Changelog**: [`0.0.4...0.0.5`](https://gitlab.com/singer-core/target-core/compare/0.0.4...0.0.5)

## [0.0.4](https://gitlab.com/singer-core/target-core/tags/0.0.4) (2022-07-19)
---

### New
* #12 S3 Save File by @omegax in !18
* #11 File Partition by @omegax in !17
* #2 Improve S3 access: Update configuration to assume role using config param `role_arn` by @omegax in !19 (Credit to [haleemur](https://github.com/haleemur) from [target-s3-jsonl](https://github.com/ome9ax/target-s3-jsonl))

### Changes

### Fixes

### Breaks

**Full Changelog**: [`0.0.3...0.0.4`](https://gitlab.com/singer-core/target-core/compare/0.0.3...0.0.4)

## [0.0.3](https://gitlab.com/singer-core/target-core/tags/0.0.3) (2022-04-30)
---

### New
* CI changebump by @omegax in !16
* #10 S3 Files Upload & #9 Asynchronous Processing Concurency Improvements by @omegax in !15

### Changes
* #2 CI/CD improvements by @omegax in !14

### Fixes

### Breaks

**Full Changelog**: [`0.0.2...0.0.3`](https://gitlab.com/singer-core/target-core/compare/0.0.2...0.0.3)

## [0.0.2](https://gitlab.com/singer-core/target-core/tags/0.0.2) (2022-04-18)

### What's Changed
* #3 Normalise the configuration: default Python outputs and better names by @omegax in !11
* #9 Asynchronous Processing by @omegax in !10
* #6 Stream data processing isolation by @omegax in !9
* #7 Static Types: Mypy testing by @omegax in !7 !8

**Full Changelog**: [`0.0.1...0.0.2`](https://gitlab.com/singer-core/target-core/compare/0.0.1...0.0.2)

## [0.0.1](https://gitlab.com/singer-core/target-core/tags/0.0.1) (2022-04-14)

### What's Changed
* #8 GitLab Pages website by @omegax in !6
* #5 Release `0.0.1` by @omegax in !5
* #4 CI/CD Static typing check by @omegax in !4
* #3 Normalise the configuration settings by @omegax in !3
* #2 Improve CI/CD settings by @omegax in !2

**Full Changelog**: [`0.0.0...0.0.1`](https://gitlab.com/singer-core/target-core/compare/0.0.0...0.0.1)

## [0.0.0](https://gitlab.com/singer-core/target-core/tags/0.0.0) (2022-04-09)

### What's Changed
* #1 [target-core] Takes on the target core features and functions to support more efficient Singer `target` package. by @omegax in !1
