Target Reference
==================

.. note::
   This project is under active development.

Module contents
---------------

.. automodule:: target
   :members:
   :undoc-members:
   :show-inheritance:

target.stream module
--------------------

.. automodule:: target.stream
   :members:
   :undoc-members:
   :show-inheritance:

target.file module
------------------

.. automodule:: target.file
   :members:
   :undoc-members:
   :show-inheritance:

target.api module
-----------------

.. automodule:: target.api
   :members:
   :undoc-members:
   :show-inheritance:
