from io import BufferedReader, TextIOWrapper
import sys
from typing import Any, Dict, List  # , Iterable
# NOTE: from pprint import pprint; pprint([json.loads(o) for o in captured.out.split('\n')[:-1]], width=152)
from pathlib import Path
import json
import datetime
import argparse

from pytest import fixture  # , mark
# from target import api
# from target.api import Extractor, set_streams, discover, sync, main
from target.api import Loader, format_data, set_persist, save_json, main  # , APILoader
from target.api.client import Client


@fixture
def config_raw(temp_path: Path, host: str) -> Dict:
    '''Use custom configuration set'''

    return {
        'user_agent': 'Eddy (eddy@acme.com)',
        'rate_limit': 99,
        'rate_period': 1.0,
        'url': f'https://{host}/v3.9',
        'add_metadata_columns': False,
        'work_dir': f'{temp_path}/tests/output',
        'post_params': {
            'locations': {
                'params': {},
                'endpoint': 'locations'
            }
        },
        'put_params': {
            'locations': {
                'params': {},
                'endpoint': 'locations/{id}'
            }
        }
    }


@fixture
def config(patch_datetime, config_raw: Dict) -> Dict:
    '''Use custom configuration set'''

    return config_raw | {
        # 'date_time': dt.strptime('2021-08-11 06:39:38.321056+00:00', '%Y-%m-%d %H:%M:%S.%f%z'),
        'date_time': datetime.datetime.now()
    }


@fixture
def patch_argument_parser(monkeypatch, temp_path: Path, config_raw: Dict) -> None:

    with open(temp_path.join('config.json'), mode='w+t') as temp_file:
        temp_file.seek(0)
        temp_file.write(json.dumps(config_raw))
        temp_file.flush()

        class argument_parser:

            def __init__(self):
                self.config = str(temp_file.name)

            def add_argument(self, x, y, help='Dummy config file', required=False):
                pass

            def parse_args(self):
                return self

        monkeypatch.setattr(argparse, 'ArgumentParser', argument_parser)


@fixture
def locations() -> List[Dict[str, Any]]:

    return [
        {'type': 'STATE', 'value': {'currently_syncing': 'locations'}},
        {'bookmark_properties': ['updated_at'],
         'key_properties': ['id'],
         'schema': {'additionalProperties': False,
                    'properties': {'created_at': {'type': ['null', 'integer']},
                                   'id': {'type': ['null', 'string']},
                                   'name': {'type': ['null', 'string']},
                                   'parent_id': {'type': ['null', 'string']},
                                   'updated_at': {'type': ['null', 'integer']}},
                    'type': ['null', 'object']},
         'stream': 'locations',
         'type': 'SCHEMA'},
        {'record': {'created_at': 1649839191, 'id': '1', 'name': 'Everywhere', 'parent_id': None, 'updated_at': 1649839191},
         'stream': 'locations',
         'time_extracted': '2022-04-29T06:39:38.321056Z',
         'type': 'RECORD',
         'version': 1651210778321},
        {'record': {'created_at': 1649839217, 'id': '2', 'name': 'Nowhere', 'parent_id': '1', 'updated_at': 1649839217},
         'stream': 'locations',
         'time_extracted': '2022-04-29T06:39:38.321056Z',
         'type': 'RECORD',
         'version': 1651210778321},
        {'type': 'STATE', 'value': {'bookmarks': {'locations': {'updated_at': 1649839217, 'version': 1651210778321}}, 'currently_syncing': 'locations'}}]


@fixture  # (scope='module')
def patch_sys_stdin_empty(monkeypatch, temp_path: Path) -> None:

    file_path = temp_path.join('input.json')
    file_path.write_text('', encoding='utf-8')

    input_file = file_path.open('rb')
    buffered = BufferedReader(input_file, buffer_size=4096)
    decoded = TextIOWrapper(buffered, encoding='utf-8')

    monkeypatch.setattr(sys, 'stdin', decoded)


@fixture  # (scope='module')
def patch_sys_stdin(monkeypatch, temp_path: Path, locations: List) -> None:

    content = '\n'.join(map(json.dumps, locations)) + '\n'
    file_path = temp_path.join('input.json')
    file_path.write_text(content, encoding='utf-8')

    input_file = file_path.open('rb')
    buffered = BufferedReader(input_file, buffer_size=4096)
    decoded = TextIOWrapper(buffered, encoding='utf-8')

    monkeypatch.setattr(sys, 'stdin', decoded)


@fixture
def state() -> Dict:
    '''Use expected state'''

    return {
        'currently_syncing': None,
        'bookmarks': {
            'locations': {'initial_full_table_complete': True}}}


@fixture
def stream_data(config: Dict, locations: List) -> Dict:
    '''Use custom configuration set'''

    return {
        'locations': {
            'schema': locations[1]['schema'],
            'endpoint': config.get('post_params', {}).get('locations', {}).get('endpoint', ''),
            'params': config.get('post_params', {}).get('locations', {}).get('params', ''),
            'format_data': format_data,
            'api_data': []}
    }


def test_set_persist(config: Dict, stream_data: Dict, locations: List) -> None:

    assert set_persist('locations', config, {}, locations[1]['schema']) == stream_data
    assert set_persist('locations', config, stream_data, locations[1]['schema']) == stream_data


async def test_save_json(patch_session, patch_datetime, config: Dict, locations: List, stream_data: Dict) -> None:
    response = await save_json('locations', stream_data, config | {'client': Client(config)}, locations[2]['record'])
    assert response == {'id': 3, 'created_at': 1649839191, 'updated_at': 1649839191, 'name': 'Everywhere', 'parent_id': None}


async def test_apiloader_sync(caplog, patch_datetime, patch_sys_stdin, patch_session, config: Dict) -> None:
    await Loader(config | {'client': Client(config)}, set_schemas=set_persist, writeline=save_json).sync(sys.stdin)
    logs = caplog.text
    assert '"POST /v3.9/locations HTTP/1.1" 200' in logs


def test_apiloader_run(capsys, patch_sys_stdin_empty, patch_session, patch_datetime, config: Dict, state: Dict) -> None:

    Loader(config | {'client': Client(config)}, set_schemas=set_persist, writeline=save_json).run(sys.stdin)

    captured = capsys.readouterr()
    assert captured.out == ''


def test_main(capsys, patch_sys_stdin_empty, patch_datetime, patch_argument_parser, patch_session, state: Dict) -> None:
    '''TEST : simple main call'''

    main(lines=sys.stdin)

    captured = capsys.readouterr()
    assert captured.out == ''
