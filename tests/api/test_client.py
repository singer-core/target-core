from typing import Any, Dict
from functools import partial
import json
from pytest import raises

from aiohttp import ClientSession, ClientResponseError

from target.api.client import Client, is_missing_status, raise_for_status, format_response
from .conftest import AppApi, TOKEN


async def test_is_missing_status(client_session: ClientSession, host: str) -> None:

    async with client_session.get(f'https://{host}/v3.9/users/0/tags') as response:

        try:
            raise_for_status(response, 'Not Found')
        except ClientResponseError as error:
            assert is_missing_status([429])(error)

    async with client_session.get(f'https://{host}/v3.9/request_status?status=429') as response:

        try:
            raise_for_status(response, 'Too Many')
        except ClientResponseError as error:
            assert not is_missing_status([429])(error)


async def test_raise_for_status(client_session: ClientSession, host: str) -> None:

    async with client_session.get(f'https://{host}/v3.9/users/0/tags') as response:
        assert response.status == 404

        with raises(ClientResponseError):
            raise_for_status(response, 'users/0/tags')

    async with client_session.get(f'https://{host}/v3.9/request_status?status=429') as response:
        with raises(ClientResponseError):
            raise_for_status(response, 'too_many')

    async with client_session.get(f'https://{host}/v3.9/request_status?status=503') as response:
        with raises(ClientResponseError):
            raise_for_status(response, 'too_many')

    async with client_session.get(f'https://{host}/v3.9/request_status?status=410') as response:
        with raises(ClientResponseError):
            raise_for_status(response, 'gone')


async def test_client_session_request(client_session: ClientSession, host: str, app_api: AppApi) -> None:

    async with client_session.request('GET', f'https://{host}/v3.9/locations') as response:
        assert response.status == 200
        resp_json = await response.json()
        assert resp_json == app_api.locations

    async with client_session.request('GET', f'https://{host}/v3.9/params?offset=99&limit=0') as response:
        assert response.status == 200
        resp_text = await response.text()
        assert resp_text == 'offset: 99, limit: 0'

    # with raises(Exception):
    #     async with client_session.request('GET', f'https://{host}/wtf') as response:
    #         assert response.status == 404


async def test_client_session_get(client_session: ClientSession, host: str, app_api: AppApi) -> None:

    async with client_session.get(f'https://{host}/v3.9/me', params={'access_token': TOKEN}) as response:
        assert response.status == 200
        r = await response.json()
        assert r == app_api.me

    async with client_session.get(f'https://{host}/v3.9/me/friends', params={'access_token': TOKEN}) as response:
        assert response.status == 200
        r = await response.json()
        assert r == app_api.my_friends


async def test_client_session_post(patch_datetime, client_session: ClientSession, host: str, app_api: AppApi) -> None:

    input_data: dict = {'name': 'Inner', 'parent_id': None}
    output_extra: dict = {'id': 3, 'created_at': 1628663978, 'updated_at': 1628663978}

    async with client_session.post(f'https://{host}/v3.9/locations', headers={'Accept': 'application/json'}, data=json.dumps(input_data)) as response:
        assert response.status == 200
        r = await response.json()
        assert r == output_extra | input_data

    async with client_session.post(f'https://{host}/v3.9/locations', json=input_data) as response:
        assert response.status == 200
        r = await response.json()
        assert r == output_extra | input_data


async def test_client_session_put(patch_datetime, client_session: ClientSession, host: str, app_api: AppApi) -> None:

    input_data: dict = {'parent_id': None}
    output_extra: dict = {'updated_at': 1628663978}
    async with client_session.put(f'https://{host}/v3.9/locations/2', json=input_data) as response:
        assert response.status == 200
        r = await response.json()
        assert r == app_api.locations['locations']['2'] | input_data | output_extra


async def test_client_get(patch_session: Any, config: Dict[str, Any], app_api: AppApi) -> None:

    async with Client(config | {'rate_limit': 1, 'rate_period': 0.009}) as client:
        response = await client.get('users', params={'offset': 9, 'limit': 3})
        assert response == app_api.no_users
        response = await client.get('tags', endpoint='users/1/tags')
        assert response == app_api.tags_1
        response = await client.get('users')
        assert response == app_api.users_0
        response = await client.get('tags', endpoint='users/0/tags', ignore_status=[404])
        assert response == {}

        # response = await client.get('errors', url='too_many', ignore_status=[429])
        # assert response == {}


async def test_client_post(patch_session: Any, config: Dict[str, Any], app_api: AppApi) -> None:

    input_data: dict = {'name': 'Inner', 'parent_id': None}
    output_extra: dict = {'id': 3, 'created_at': 1628663978, 'updated_at': 1628663978}

    async with Client(config) as client:
        response = await client.post('locations', endpoint='locations', data=input_data)
        assert response == output_extra | input_data

        response = await client.post('locations', endpoint='locations/0', data=input_data, ignore_status=[404])
        assert response == {}


async def test_client_get_records(patch_session: Any, config: Dict[str, Any], app_api: AppApi) -> None:
    async with Client(config) as client:
        response = await client.get_records('locations', format_response=partial(format_response, params={'path': 'locations'}))
        assert response == format_response((app_api.locations,), {'path': 'locations'})
