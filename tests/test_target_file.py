'''Tests for the target.file module'''
# Standard library imports
from pathlib import Path
import datetime
import gzip
import lzma
import json
from copy import deepcopy
from typing import Any, Dict
from pytest import raises, mark

from target.file import config_file, config_compression, _get_relative_path, set_schema, save_json
from target._logger import get_logger

from .test_target_stream import clear_dir

LOGGER = get_logger()


def test_config_file(monkeypatch, patch_datetime, config_raw, config):
    '''TEST : extract and enrich the configuration'''

    config.pop('open_func')
    assert config_file(config_raw) == config

    file_size: int = 5122903040
    config_file_size: dict = config_file(config_raw | {'file_size': file_size})
    config.pop('memory_buffer')
    assert config_file_size == config | {
        'file_size': file_size
    }


def test_config_compression(config):
    '''TEST : extract and enrich the configuration'''

    assert f"{config.get('compression')}".lower() in {'', 'none', 'gzip', 'lzma'}

    with raises(NotImplementedError):

        config_compression(config_file({'compression': 'dummy'}))


@mark.parametrize("compression,extention,open_func", [('none', '', open), ('gzip', '.gz', gzip.open), ('lzma', '.xz', lzma.open)])
def test_config_compression_open_func(monkeypatch, patch_datetime, temp_path, config, compression, extention, open_func):
    '''TEST : extract and enrich the configuration'''

    assert config_compression(config_file({'compression': compression, 'work_dir': str(Path(temp_path))})) == {
        'date_time': config['date_time'].astimezone(datetime.timezone.utc).replace(tzinfo=None),
        'memory_buffer': 64e6,
        'work_dir': str(Path(temp_path)),
        'work_path': Path(temp_path),
        'compression': compression,
        'path_template': '{stream}-{date_time}.json' + extention,
        'open_func': open_func
    }


def test_get_relative_path(config):
    '''TEST : simple _get_relative_path call'''

    date_time = datetime.datetime.strptime('2022-04-07 06:25:44.423099', '%Y-%m-%d %H:%M:%S.%f')
    assert _get_relative_path('dummy_stream', config, date_time=date_time) == 'dummy_stream-2022-04-07 06:25:44.423099.json'

    config.update(path_template='{stream:_>8}/{date_time:%Y}/{date_time:%m}/{date_time:%d}/{date_time:%Y%m%d_%H%M%S_%f}_part_{part:0>3}.json')
    assert _get_relative_path('my', config, date_time=date_time, part=9) == '______my/2022/04/07/20220407_062544_423099_part_009.json'


def test_set_schema_file(config):
    '''TEST : simple set_schema call'''

    file_data = {}
    set_schema('dummy_stream', config, file_data)

    assert file_data == {
        'dummy_stream': {
            'part': 1,
            'file_data': [],
            'path': {
                1: {
                    'relative_path': 'dummy_stream-2022-04-29 07:39:38.321056+01:00.json',
                    'absolute_path': config['work_path'] / 'dummy_stream-2022-04-29 07:39:38.321056+01:00.json'}}}}


async def test_save_json(caplog, config, file_metadata):
    '''TEST : simple save_json call'''
    Path(config['work_dir']).mkdir(parents=True, exist_ok=True)

    # NOTE: Test saved file compression
    for open_func, extension in {open: '', gzip.open: '.gz', lzma.open: '.xz'}.items():
        file_metadata_copy = deepcopy(file_metadata)

        for stream, file_info in file_metadata_copy.items():
            file_info['path'][1]['absolute_path'].unlink(missing_ok=True)
            file_info['path'][1]['absolute_path'] = file_info['path'][1]['absolute_path'].with_name(file_info['path'][1]['absolute_path'].name + extension)
            await save_json(stream, deepcopy(file_metadata_copy), config | {'open_func': open_func})

        assert not file_metadata_copy['tap_dummy_test-test_table_one']['path'][1]['absolute_path'].exists()
        assert not file_metadata_copy['tap_dummy_test-test_table_two']['path'][1]['absolute_path'].exists()
        assert file_metadata_copy['tap_dummy_test-test_table_three']['path'][1]['absolute_path'].exists()

        with open_func(file_metadata_copy['tap_dummy_test-test_table_three']['path'][1]['absolute_path'], 'rt', encoding='utf-8') as input_file:
            assert [item for item in input_file] == [json.dumps(item) + '\n' for item in file_metadata['tap_dummy_test-test_table_three']['file_data']]

    # NOTE: Test memory buffer post_processing
    async def post_processing(config: Dict[str, Any], file_metadata: Dict) -> None:
        LOGGER.info("[POST PROCESSING] File '%s' saved using open_func '%s'",
                    file_metadata['absolute_path'], config['open_func'].__name__)

    file_metadata_copy = deepcopy(file_metadata)

    for stream, file_info in file_metadata_copy.items():
        file_info['path'][1]['absolute_path'].unlink(missing_ok=True)
        await save_json(stream, deepcopy(file_metadata_copy), config, post_processing=post_processing)

    assert not file_metadata_copy['tap_dummy_test-test_table_one']['path'][1]['absolute_path'].exists()
    assert not file_metadata_copy['tap_dummy_test-test_table_two']['path'][1]['absolute_path'].exists()
    assert file_metadata_copy['tap_dummy_test-test_table_three']['path'][1]['absolute_path'].exists()

    # NOTE: Test Parts
    file_size: int = 99
    config.pop('memory_buffer')
    config_part = config | {
        'file_size': file_size,
        'path_template': '{stream}-{date_time}_part_{part:0>3}.json',
    }
    stream = 'hot_stream'
    file_metadata_part = {}
    set_schema('hot_stream', config_part, file_metadata_part)

    for record in file_metadata['tap_dummy_test-test_table_three']['file_data']:
        await save_json(stream, file_metadata_part, config_part, record)
    await save_json(stream, file_metadata_part, config_part)

    assert len(file_metadata_part[stream]['path']) == 2
    assert file_metadata_part[stream]['path'][1]['absolute_path'].stat().st_size == 128
    assert file_metadata_part[stream]['path'][2]['absolute_path'].stat().st_size == 64

    assert file_metadata_part[stream]['path'][1]['absolute_path'].read_text(encoding='utf-8')[:-1].split('\n') \
        == [json.dumps(item) for item in file_metadata['tap_dummy_test-test_table_three']['file_data'][:2]]

    assert file_metadata_part[stream]['path'][2]['absolute_path'].read_text(encoding='utf-8')[:-1].split('\n') \
        == [json.dumps(item) for item in file_metadata['tap_dummy_test-test_table_three']['file_data'][2:]]

    file_metadata_part[stream]['path'][1]['absolute_path'].unlink(missing_ok=True)
    file_metadata_part[stream]['path'][2]['absolute_path'].unlink(missing_ok=True)

    # NOTE: Test file size limit post_processing
    file_metadata_part = {}
    set_schema('hot_stream', config_part, file_metadata_part)

    for record in file_metadata['tap_dummy_test-test_table_three']['file_data']:
        await save_json(stream, file_metadata_part, config_part, record, post_processing=post_processing)
    await save_json(stream, file_metadata_part, config_part, post_processing=post_processing)

    assert "[POST PROCESSING] File '{}' saved using open_func '{}'".format(
        file_metadata_part[stream]['path'][2]['absolute_path'], config['open_func'].__name__) in caplog.text

    assert len(file_metadata_part[stream]['path']) == 2
    assert file_metadata_part[stream]['path'][1]['absolute_path'].stat().st_size == 128
    assert file_metadata_part[stream]['path'][2]['absolute_path'].stat().st_size == 64

    clear_dir(Path(config['work_dir']))
